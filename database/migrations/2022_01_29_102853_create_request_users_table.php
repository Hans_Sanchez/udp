<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('document_code');
            $table->string('document_label');
            $table->string('r_to_c');
            $table->string('s_to_c');
            $table->string('o_to_c');
            $table->string('f_to_c');
            $table->string('r_to_u');
            $table->string('s_to_u');
            $table->string('o_to_u');
            $table->string('f_to_u');
            $table->string('check_code')->nullable();
            $table->string('check_label')->nullable();
            $table->string('pass_code')->nullable();
            $table->string('pass_label')->nullable();
            $table->string('elaborate')->nullable();
            $table->enum('status', ['approved', 'rejected', 'in_review'])
                ->default('in_review');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_users');
    }
}
