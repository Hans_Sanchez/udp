<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Breadcrumbs extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $level1, $level2, $level3, $level4, $active1, $active2, $active3, $active4;


    public function __construct
    (
        $level1 = NULL, $level2 = NULL, $level3 = NULL, $level4 = NULL,
        $active1 = NULL, $active2 = NULL, $active3 = NULL, $active4 = NULL
    )
    {
        $this->level1 = $level1;
        $this->level2 = $level2;
        $this->level3 = $level3;
        $this->level4 = $level4;

        $this->active1 = $active1;
        $this->active2 = $active2;
        $this->active3 = $active3;
        $this->active4 = $active4;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.breadcrumbs');
    }
}
