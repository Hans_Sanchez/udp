<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'document_code',
        'document_label',
        'r_to_c',
        's_to_c',
        'o_to_c',
        'f_to_c',
        'r_to_u',
        's_to_u',
        'o_to_u',
        'f_to_u',
        'check_code',
        'check_label',
        'pass_code',
        'pass_label',
        'elaborate',
        'status'
    ];

    protected $appends = ['StatusLabel'];

    public function scopeSearch($query, $search_term)
    {
        if ($search_term != null)
            $query->where('id', 'like', '%' . $search_term . '%')
                ->where('created_at', 'like', '%' . $search_term . '%');
    }

    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 'in_review':
                return '<span class="badge badge-info full-20">
                            <strong>EN REVISIÓN</strong>
                        </span>';
            case 'approved':
                return '<span class="badge badge-success full-20">
                            <strong>APROBADO</strong>
                        </span>';
            case 'rejected':
                return '<span class="badge badge-danger full-20">
                            <strong>RECHAZADO</strong>
                        </span>';
            default:
                return '<span class="badge badge-danger full-20">
                            <strong>' . $this->status . '</strong>
                        </span>';
        }
    }
}
