<?php

namespace App\Http\Middleware;

use App\Models\AssigmentTask;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class DrawMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
    }
}
