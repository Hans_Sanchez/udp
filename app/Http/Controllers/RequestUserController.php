<?php

namespace App\Http\Controllers;

use App\Models\RequestUser;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RequestUserController extends Controller
{
    public function getRequest(Request $request): \Illuminate\Http\JsonResponse
    {
        $search = $request->search;
        $RequestUsers = RequestUser::search($search)->orderBy('created_at', 'DESC')->simplePaginate(10);
        return response()->json(['RequestUsers' => $RequestUsers]);
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $requestUser = new RequestUser($request->all());
            $requestUser->name = mb_strtoupper($request->name);
            $requestUser->document_code = $request->document['code'];
            $requestUser->document_label = mb_strtolower($request->document['label']);
            $requestUser->elaborate = mb_strtoupper(auth()->user()->name);
            $requestUser->save();
            return response()->json(['msg' => 'Se guardó con éxito', 'icon' => 'success', 'requestUser' => $requestUser, 'new' => true]);
        } catch (\Exception $exception) {
            return response()->json(['msg' => $exception->getMessage(), 'icon' => 'error'], 500);
        }
    }

    public function update(Request $request, RequestUser $requestUser): \Illuminate\Http\JsonResponse
    {
        try {
            $requestUser->update($request->all());
            $requestUser->name = mb_strtoupper($request->name);
            $requestUser->document_code = $request->document['code'];
            $requestUser->document_label = mb_strtolower($request->document['label']);
            if ($request->check !== null) {
                $requestUser->check_code = $request->check['code'];
                $requestUser->check_label = $request->check['label'];
            }
            if ($request->pass !== null) {
                $requestUser->pass_code = $request->pass['code'];
                $requestUser->pass_label = $request->pass['label'];
            }
            $requestUser->update();
            return response()->json(['msg' => 'Se actualizó con éxito', 'icon' => 'success', 'requestUser' => $requestUser, 'new' => false]);
        } catch (\Exception $exception) {
            return response()->json(['msg' => $exception->getMessage(), 'icon' => 'error'], 500);
        }
    }

    public function generatePDF(RequestUser $requestUser)
    {
        $img1 = file_get_contents(public_path() . '/assets/img/logos/logo_registraduria.png');
        $logo = 'data:image/png' . ';base64,' . base64_encode($img1);
        $pdf = PDF::setOptions(['logOutputFile' => storage_path('logs/log.htm'), 'tempDir' => storage_path('logs/')])
            ->loadView('documents.requestUsers', compact('requestUser', 'logo'));
        $pdf->setPaper('letter', 'portrait'); // portrait = vertical | , landscape = horizontal --
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $font = $dom_pdf->getFontMetrics()->get_font("Arial");
        $canvas->page_text(265, 800, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 9, array(0.565, 0.565, 0.565));
        $pdf->output();
        $name = 'SOLICITUD-NÚMERO-' . $requestUser->id . '-DE-' . mb_strtoupper(Str::slug($requestUser->name)) . '.pdf';
        Storage::put('public/documents/' . $name, $pdf->output());
        return $pdf->stream($name);
    }

}
