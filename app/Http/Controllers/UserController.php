<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Imports\UsersImport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{

    public function getUsers(Request $request)
    {
        $search = $request->search;
        $users = User::search($search)
            ->orderBy('personal_id', 'DESC')
            ->simplePaginate(10);

        return response()->json(['users' => $users]);
    }

    public function getUsersList(Request $request)
    {
        $search = $request->search;

        return User::select('id AS code', 'name AS label')
            ->where('users.id', 'LIKE', '%' . $search . '%')
            ->orderBy('users.id', 'DESC')
            ->get();
    }

    public function store(UserRequest $request)
    {
        try {
            $user = new User($request->all());
            $this->extracted($request, $user);
            $user->save();
            return response()->json(['msg' => 'Se guardó con éxito', 'icon' => 'success', 'user' => $user, 'new' => true]);
        } catch (\Exception $exception) {
            return response()->json(['msg' => $exception->getMessage(), 'icon' => 'error'], 500);
        }
    }

    public function update(UserRequest $request, User $user)
    {
        try {
            $this->extracted($request, $user);
            $user->update();
            return response()->json(['msg' => 'Se actualizó con éxito', 'icon' => 'success', 'user' => $user, 'new' => false]);
        } catch (\Exception $exception) {
            return response()->json(['msg' => $exception->getMessage(), 'icon' => 'error'], 500);
        }

    }

    public function extracted(UserRequest $request, User $user): void
    {
        $user->name = mb_strtoupper($request->name);
        $user->email = $request->email;
    }
}
