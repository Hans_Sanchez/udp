<x-app-layout>
    <div class="card">
        <div class="card-header bg-espumados text-uppercase">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb m-0 p-0" style="border: none !important;">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('home') }}">
                            Tablero de control
                        </a>
                    </li>
                </ol>
            </nav>
        </div>
        <div class="pl-4 pr-4 pt-4">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <router-link :to="{ name:'assignments' }" @click="show = !show">
                        <div class="card text-white bg-info">
                            <div class="card-body">
                                <h3 class="mb-0">
                                    <i class="fas fa-circle text-white"></i>
                                    <b>ASIGNADAS: {{ $assigment }}</b>
                                </h3>
                            </div>
                        </div>
                    </router-link>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <router-link :to="{ name:'in_process' }" @click="show = !show">
                        <div class="card text-white bg-warning">
                            <div class="card-body">
                                <h3 class="mb-0">
                                    <i class="fas fa-circle text-white"></i>
                                    <b>EN PROCESO: {{ $inProcess }}</b>
                                </h3>
                            </div>
                        </div>
                    </router-link>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <router-link :to="{ name:'partials' }" @click="show = !show">
                        <div class="card text-white bg-orange-card">
                            <div class="card-body">
                                <h3 class="mb-0">
                                    <i class="fas fa-circle text-white"></i>
                                    <b>PARCIALES: {{ $partials }}</b>
                                </h3>
                            </div>
                        </div>
                    </router-link>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <router-link :to="{ name:'finishes' }" @click="show = !show">
                        <div class="card text-white bg-success">
                            <div class="card-body">
                                <h3 class="mb-0">
                                    <i class="fas fa-circle text-white"></i>
                                    <b>FINALIZADAS: {{ $finished }}</b>
                                </h3>
                            </div>
                        </div>
                    </router-link>
                </div>
            </div>
        </div>
        <div class="card-body">

        </div>
    </div>
</x-app-layout>
<script>
    //Cuando la página esté cargada completamente
    $(document).ready(function () {
        setTimeout(refresh, 10000); //Cada 60 segundos (60000 milisegundos) se ejecutará la función refrescar
    });

    function refresh() {
        if (window.location.pathname === '/home') {
            window.location.reload(); //Actualiza la página
        }
    }
</script>
