<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
            data-class="c-sidebar-show">
        <i class="fas fa-bars c-icon c-icon-lg"></i>
    </button>
    <div>
        <a class="c-header-brand d-lg-none text-center justify-content-center" href="#">
            <b style="font-size: 25px">UDP</b>
        </a>
    </div>
    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
            data-class="c-sidebar-lg-show" responsive="true">
        <i class="fas fa-bars c-icon c-icon-lg"></i>
    </button>
    <ul class="c-header-nav ml-auto mr-4">
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">
                <div class="c-avatar">
                    <img class="c-avatar-img" src="{{ asset('assets/img/avatars/pngwing.com.png') }}"
                         alt="user@email.com"/>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
                <div class="dropdown-header bg-light py-2">
                    <strong>Mi cuenta</strong>
                </div>
                <a class="dropdown-item text-uppercase" href="{{ route('profile.show') }}">
                    <i class="fas fa-user c-icon mr-2"></i>
                    {{ auth()->user()->name }}
                </a>
                <a class="dropdown-item" href="{{ route('logout') }}">
                    <i class="fas fa-sign-out-alt c-icon mr-2"></i>
                    Cerrar sesión
                </a>
                {{--                <div class="dropdown-header bg-light py-2">--}}
                {{--                    <strong>Settings</strong>--}}
                {{--                </div>--}}
                {{--                <div class="dropdown-divider"></div>--}}
            </div>
        </li>
    </ul>
</header>
