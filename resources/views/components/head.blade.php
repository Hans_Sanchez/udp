<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>UDP</title>

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/config_100.png') }}">
    <link rel="manifest" href="{{ asset('assets/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/img/config_100.png') }}">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert2/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.min.css')}}">
    <!-- Main styles for this application-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #e1e1e1 !important;
            border-radius: 4px;
            height: 37px !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 28px;
            margin: 4px !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 26px;
            position: absolute;
            top: 1px;
            right: 1px;
            width: 20px;
            margin-top: 4px;
        }

        .bv-modal{
            background: #e1e1e1 !important;
        }

        .v-select,
        .v-select * {
            box-sizing: border-box;
            background: #FFFFFF !important;
            color: #000000 !important;
        }
    </style>

</head>
