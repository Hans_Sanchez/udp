<footer class="c-footer">
    <div>
        <a href="#"><b>GRUPO ESPUMADOS</b></a>
        <b>&copy; {{ now()->format('Y') }}</b>
    </div>
    <div class="ml-auto">
        <a href="#"><b>Versión 1.1.6</b></a>
    </div>
</footer>
