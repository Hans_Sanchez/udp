@php
    // Roles
    $admin = auth()->user()->hasRole('admin');
    $user = auth()->user()->hasRole('user');
@endphp

<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <b class="c-sidebar-brand-full" style="font-size: 25px">UDP</b>
    </div>
    <ul class="c-sidebar-nav">
        @if($admin)
            <li class="c-sidebar-nav-title">Tablero de control</li>
            <li class="c-sidebar-nav-item">
                <router-link :to="{ name:'home' }" class="c-sidebar-nav-link" @click="show = !show">
                    <i class="fas fa-tachometer-alt c-sidebar-nav-icon"></i>
                    Home
                </router-link>
            </li>
            <li class="c-sidebar-nav-title">Administración</li>
            <li class="c-sidebar-nav-item">
                <router-link :to="{ name:'users' }" class="c-sidebar-nav-link" @click="show = !show">
                    <i class="fas fa-users c-sidebar-nav-icon"></i>
                    Usuarios
                </router-link>
            </li>
            <li class="c-sidebar-nav-item">
                <router-link :to="{ name:'requests' }" class="c-sidebar-nav-link" @click="show = !show">
                    <i class="fas fa-clipboard-list c-sidebar-nav-icon"></i>
                    Solicitudes
                </router-link>
            </li>
        @endif
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
</div>
