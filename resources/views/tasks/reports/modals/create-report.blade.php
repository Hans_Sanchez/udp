<!-- Modal -->
<div class="modal fade" id="reportTask-{{ $assigmentTask->id }}" tabindex="-1" aria-labelledby="reportTask-{{ $assigmentTask->id }}-Label" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title bg-espumados text-uppercase"
                    id="reportTask-{{ $assigmentTask->id }}-Label">
                    Reporte para la tarea: #{{ $assigmentTask->id }}
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('tasks.reports.store') }}" method='post'>
                @csrf
                <div class="modal-body bv-modal" style="text-align: left !important;">
                    <div class="row">
                        @include('reminder')
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Cantidad entregada <span class="text-danger">( * )</span>
                                    </b>
                                </label>
                                <input type="hidden" value="{{ $assigmentTask->id }}" name="assigment_task_id">
                                <input type="number"
                                       name="delivery_quantity"
                                       id="delivery_quantity"
                                       class="form-control"
                                       value="{{ old('delivery_quantity') }}"
                                       placeholder="Cantidad entregada" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Densidad <span class="text-danger">( * )</span>
                                    </b>
                                </label>
                                <input type="number"
                                       name="density"
                                       id="density"
                                       class="form-control"
                                       step="any"
                                       value="{{ old('density') }}"
                                       placeholder="Densidad del material" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Ancho <span class="text-danger">( * )</span>
                                    </b>
                                </label>
                                <input type="number"
                                       name="width"
                                       id="width"
                                       class="form-control"
                                       value="{{ old('width') }}"
                                       placeholder="Ancho del material" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Largo <span class="text-danger">( * )</span>
                                    </b>
                                </label>
                                <input type="number"
                                       name="long"
                                       id="long"
                                       class="form-control"
                                       value="{{ old('long') }}"
                                       placeholder="Largo del material" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Alto <span class="text-danger">( * )</span>
                                    </b>
                                </label>
                                <input type="number"
                                       name="height"
                                       id="height"
                                       class="form-control"
                                       value="{{ old('height') }}"
                                       placeholder="Alto del material" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Número de bloques <span class="text-danger">( * )</span>
                                    </b>
                                </label>
                                <input type="number"
                                       name="number_of_blocks"
                                       id="number_of_blocks"
                                       class="form-control"
                                       value="{{ old('number_of_blocks') }}"
                                       placeholder="Número de bloques utilizados" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Lote <span class="text-danger">( * )</span>
                                    </b>
                                </label>
                                <input type="number"
                                       name="lot"
                                       id="lot"
                                       class="form-control"
                                       value="{{ old('lot') }}"
                                       placeholder="Lote del cual es procedente" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Kilos de retal
                                    </b>
                                </label>
                                <input type="number"
                                       name="kilos_of_scrap"
                                       id="kilos_of_scrap"
                                       class="form-control"
                                       value="{{ old('kilos_of_scrap') }}"
                                       placeholder="Kilos de retal generados">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Kilos de cueros
                                    </b>
                                </label>
                                <input type="number"
                                       name="kilos_of_leather"
                                       id="kilos_of_leather"
                                       class="form-control"
                                       value="{{ old('kilos_of_leather') }}"
                                       placeholder="Kilos de cueros generados">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Kilos de segundas
                                    </b>
                                </label>
                                <input type="number"
                                       name="kilos_of_seconds"
                                       id="kilos_of_seconds"
                                       class="form-control"
                                       value="{{ old('kilos_of_seconds') }}"
                                       placeholder="Kilos de segundas generados">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <b>
                                        Observaciones
                                    </b>
                                </label>
                                <br>
                                <small @class('text-danger')>
                                    Este campo es para reportar comentarios adicionales con respecto a dicha tarea
                                </small>
                                <textarea name="comment"
                                          @class('form-control')
                                          rows="5"
                                          id="comment">{{ old('comment') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
