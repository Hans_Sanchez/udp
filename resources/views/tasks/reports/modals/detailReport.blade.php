<!-- Modal -->
<div class="modal fade" id="detailReport-{{ $reportTask->id }}" tabindex="-1"
     aria-labelledby="detailReport-{{ $reportTask->id }}-Label" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title bg-espumados text-uppercase"
                    id="detailReport-{{ $reportTask->id }}-Label">
                    Detalle del reporte de la tarea #{{ $reportTask->assigmentTask->id }}
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="text-align: left !important;">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Cantidad entregada</b></label>
                        <p>{{ $reportTask->delivery_quantity }}</p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Densidad</b></label>
                        <p>{{ $reportTask->density }}</p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Ancho</b></label>
                        <p>{{ $reportTask->width }} CM</p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Largo</b></label>
                        <p>{{ $reportTask->long }} CM</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Alto</b></label>
                        <p>{{ $reportTask->height }} CM</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Número de bloques</b></label>
                        <p>{{ $reportTask->number_of_blocks }}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Lote</b></label>
                        <p>{{ $reportTask->lot }}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Kilos de retal</b></label>
                        <p>{{ ($reportTask->kilos_of_scrap != null) ? $reportTask->kilos_of_scrap : 0 }} KG</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Kilos de cueros</b></label>
                        <p>{{ ($reportTask->kilos_of_leather != null) ? $reportTask->kilos_of_leather : 0 }} KG</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label><b>Kilos de segundas</b></label>
                        <p>{{ ($reportTask->kilos_of_seconds != null) ? $reportTask->kilos_of_seconds : 0 }} KG</p>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label><b>Observaciones</b></label>
                        <p>{{ ($reportTask->comment != null) ? $reportTask->comment : 'Sin observaciones' }}</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
