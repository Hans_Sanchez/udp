<div class="table-responsive">
    <table id="InProcessTable" class="table table-bordered table-sm table-striped table-condensed">
        <thead>
        <tr class="bg-orange">
            <th class="tt-espumados">Tarea</th>
            <th class="tt-espumados">Estado de la maquina</th>
            <th class="tt-espumados">Reportar</th>
            <th class="tt-espumados">En proceso</th>
            <th class="tt-espumados">Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($in_process as $assigmentTask)
            <tr>
                <td class="text-center">#{{ $assigmentTask->id }}</td>
                <td class="text-center">
                    {!! $assigmentTask->machinery->status_label !!}
                </td>
                <td class="text-center">
                    <div class="btn-group">
                        <a style="cursor: pointer"
                           class="text-success"
                           data-toggle="modal"
                           data-target="#reportTask-{{ $assigmentTask->id }}"
                           title="Reportar tarea">
                            <i class="fas fa-thumbtack fa-3x"></i>
                        </a>
                        @include('tasks.reports.modals.create-report')
                    </div>
                </td>
                <td class="text-center">
                    <span class="text-warning">
                        <i class="fas fa-check-square fa-3x"></i>
                    </span>
                </td>
                <td class="text-center justify-content-center" width="120px">
                    <div class="btn-group">
                        <a style="cursor: pointer"
                           class="text-info"
                           data-toggle="modal"
                           data-target="#detailTask-{{ $assigmentTask->id }}"
                           title="Ver">
                            <i class="fas fa-eye fa-3x"></i>
                        </a>
                        @include('tasks.assignments.detail-task')
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>
