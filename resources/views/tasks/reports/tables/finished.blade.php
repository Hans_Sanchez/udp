<div class="table-responsive">
    <table id="reportTable" class="table table-bordered table-sm table-striped table-condensed">
        <thead>
        <tr class="bg-orange">
            <th class="tt-espumados">Identificador</th>
            <th class="tt-espumados">Prioridad</th>
            <th class="tt-espumados">Comentarios</th>
            <th class="tt-espumados">Finalizado</th>
            <th class="tt-espumados">Acciones</th>

        </tr>
        </thead>
        <tbody>
        @foreach($reportTasks as $reportTask)
            <tr>
                <td class="text-center">#{{ $reportTask->assigmentTask->id }}</td>
                <td class="text-center">{!! $reportTask->assigmentTask->priority_label !!}</td>
                <td>{{ $reportTask->comment }}</td>
                <td class="text-center" width="35px">
                    <span class="text-success">
                        <i class="fas fa-laugh-beam fa-3x"></i>
                    </span>
                </td>
                <td class="text-center justify-content-center" width="120px">
                    <div class="btn-group">
                        <a style="cursor: pointer"
                           class="text-info"
                           data-toggle="modal"
                           data-target="#detailReport-{{ $reportTask->id }}"
                           title="Ver">
                            <i class="fas fa-eye fa-3x"></i>
                        </a>
                        @include('tasks.reports.modals.detailReport')
                        &nbsp;
                        <a style="cursor: pointer"
                           class="text-warning"
                           data-toggle="modal"
                           data-target="#editReport-{{ $reportTask->id }}"
                           title="Editar">
                            <i class="fas fa-edit fa-3x"></i>
                        </a>
{{--                        @include('tasks.assignments.edit-task')--}}
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>
