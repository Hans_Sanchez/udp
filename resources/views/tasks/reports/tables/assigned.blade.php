<div class="table-responsive">
    <table id="assignedTable" class="table table-bordered table-sm table-striped table-condensed">
        <thead>
        <tr class="bg-orange">
            <th class="tt-espumados">Identificador</th>
            <th class="tt-espumados">Maquina</th>
            <th class="tt-espumados">Material</th>
            <th class="tt-espumados">Asiganación (Fecha y Hora)</th>
            <th class="tt-espumados">Prioridad</th>
            <th class="tt-espumados">En proceso</th>
        </tr>
        </thead>
        <tbody>
        @foreach($assigned as $assigmentTask)
            <tr>
                <td class="text-center">{{ $assigmentTask->id }}</td>
                <td class="text-center">{{ $assigmentTask->machinery->name }}</td>
                <td class="text-center">{{ $assigmentTask->material->name }}</td>
                <td class="text-center">{{ $assigmentTask->created_at->format('d-m-Y g:i a') }}</td>
                <td class="text-center">{!! $assigmentTask->priority_label !!}</td>
                <td class="text-center" width="35px">
                    <div class="form-check">
                        <a href="{{ route('tasks.reports.process', $assigmentTask) }}">
                            <span class="text-danger">
                                <i class="fas fa-window-close fa-3x"></i>
                            </span>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>
