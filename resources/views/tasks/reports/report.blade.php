<x-app-layout>
    <div class="card">
        <div class="card-header bg-espumados text-uppercase">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb m-0" style="border: none !important;">
                    <li class="breadcrumb-item">
                        <a class="text-gray-500">
                            Gestión de tareas
                        </a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('tasks.reports.index') }}">
                            Reporte
                        </a>
                    </li>
                </ol>
            </nav>
        </div>
        <div class="card-body">
            <div id="accordion">
                @foreach($titles as $key => $title)
                    <div class="card">
                        <div class="card-header" id="heading-{{$key}}">
                            <h5 class="mb-0">
                                <button class="btn btn-link text-uppercase" data-toggle="collapse"
                                        data-target="#collapse-{{ $key }}"
                                        aria-expanded="true" aria-controls="collapse-{{ $key }}">
                                    <b>{{ $title }}</b>
                                </button>
                            </h5>
                        </div>

                        <div id="collapse-{{ $key }}" class="collapse" aria-labelledby="heading-{{$key}}"
                             data-parent="#accordion">
                            <div class="card-body">
                                @switch($key)
                                    @case('assigned') @include('tasks.reports.tables.assigned') @break
                                    @case('in-process') @include('tasks.reports.tables.in-process') @break
                                    @case('finished') @include('tasks.reports.tables.finished') @break
                                @endswitch
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</x-app-layout>
