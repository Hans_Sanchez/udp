<x-app-layout>
    <div class="card">
        <div class="card-header bg-espumados text-uppercase">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb m-0" style="border: none !important;">
                            <li class="breadcrumb-item">
                                <a class="text-gray-500">
                                    Gestión de tareas
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('tasks.assigment.index') }}">
                                    Asignación
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <button class="btn btn-info float-right"
                            id="refresh"
                            title="Refrescar manualmente los registros">
                        <i class="fas fa-sync"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="pl-4 pr-4 pt-4">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- Button trigger modal -->
                    <div class="btn-group">
                        <button type="button"
                                class="btn btn-primary"
                                data-target="#createMassive"
                                data-backdrop="static"
                                data-toggle="modal"
                                data-keyboard="false">
                            <b style="font-size: 16px !important;">Carga masiva</b>
                        </button>
                        {{--                        <button type="button"--}}
                        {{--                                class="btn btn-outline-primary"--}}
                        {{--                                data-toggle="modal"--}}
                        {{--                                data-target="#machinery"
                        {{--                                data-backdrop="static">--}}
                        {{--                            Maquinaria--}}
                        {{--                        </button>--}}
                        <button type="button"
                                class="btn btn-secondary"
                                data-target="#createIndividual"
                                data-backdrop="static"
                                data-toggle="modal"
                                data-keyboard="false">
                            <b style="font-size: 16px !important;">Nueva tarea</b>
                        </button>
                    </div>
                </div>
                @include('tasks.assignments.create-massive')
                @include('tasks.assignments.create-individual')
                @include('tasks.assignments.machinery')
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form class="form-inline my-2 my-lg-0 float-right">
                        <input class="form-control mr-sm-2" type="text" name="search-tasks"
                               value="{{request('search-tasks')}}" aria-label="Search">
                        <div class="btn-group">
                            <button class="btn btn-secondary my-2 my-sm-0"
                                    type="submit">
                                <b style="font-size: 16px !important;">Buscar</b>
                            </button>
                            <a href="{{route('tasks.assigment.index')}}"
                               class="btn btn-primary">
                                <b style="font-size: 16px !important;">Limpiar</b>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body" id="cardAssigment">
            <div class="table-responsive">
                <table id="assigmentTable" class="table table-bordered table-sm table-striped table-condensed">
                    <thead class="headerStatic">
                    <tr class="bg-orange">
                        <th class="tt-espumados">ID</th>
                        <th class="tt-espumados">N° Semi-Elaborado</th>
                        <th class="tt-espumados">N° Producto terminado</th>
                        <th class="tt-espumados">Nombre del cliente</th>
                        <th class="tt-espumados">Maquina</th>
                        <th class="tt-espumados">Material</th>
                        <th class="tt-espumados">Estado</th>
                        <th class="tt-espumados">Asiganación (Fecha y Hora)</th>
                        <th class="tt-espumados">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($assigmentTasks as $assigmentTask )
                        <tr>
                            <td class="text-center">{{ $assigmentTask->id }}</td>
                            <td class="text-center">{{ $assigmentTask->order_sem }}</td>
                            <td class="text-center">{{ $assigmentTask->order_pt }}</td>
                            <td class="text-uppercase">{{ $assigmentTask->client_name }}</td>
                            <td class="text-uppercase">{{ $assigmentTask->machinery->name }}</td>
                            <td class="text-uppercase">{{ $assigmentTask->material->name }}</td>
                            <td class="text-center">{!! $assigmentTask->status_label !!}</td>
                            <td class="text-center">{{ $assigmentTask->created_at->format('d-m-Y g:i a') }}</td>
                            <td class="text-center justify-content-center" width="120px">
                                <div class="btn-group">
                                    <a style="cursor: pointer"
                                       class="text-info"
                                       data-toggle="modal"
                                       data-backdrop="static"
                                       data-target="#detailTask-{{ $assigmentTask->id }}"
                                       title="Ver">
                                        <i class="fas fa-eye fa-2x"></i>
                                    </a>
                                    @include('tasks.assignments.detail-task')
                                    &nbsp;
                                    <a style="cursor: pointer"
                                       class="text-warning"
                                       data-toggle="modal"
                                       data-backdrop="static"
                                       data-target="#editTask-{{ $assigmentTask->id }}"
                                       title="Editar">
                                        <i class="fas fa-edit fa-2x"></i>
                                    </a>
                                    @include('tasks.assignments.edit-task')
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                {{$assigmentTasks->appends(request()->all())->render()}}
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $('#refresh').click(function () {
        window.location.reload();
    });
</script>
