<!-- Modal -->
<div class="modal fade" id="createIndividual" tabindex="-1" aria-labelledby="createIndividualLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title bg-espumados text-uppercase" id="createIndividualLabel">
                    Nueva tarea
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('tasks.assigment.store') }}" method="POST" autocomplete="off">
                @csrf
                <div class="modal-body bv-modal">
                    <div class="row">
                        @include('reminder')
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <h5>
                                        <b>
                                            O - Semi-Elaborados <span class="text-danger">( * )</span>
                                        </b>
                                    </h5>
                                </label>
                                <input type="number"
                                       name="order_sem"
                                       id="order_sem"
                                       class="form-control"
                                       value="{{ old('order_sem') }}"
                                       placeholder="Número de orden del semi-elaborado"
                                       required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="blq">
                                    <h5>
                                        <b>
                                            BLQ <span class="text-danger">( * )</span>
                                        </b>
                                    </h5>
                                </label>
                                <input type="number"
                                       name="blq"
                                       id="blq"
                                       class="form-control"
                                       value="{{ old('blq') }}"
                                       placeholder="Bloque" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_sem">
                                    <h5>
                                        <b>
                                            CPC <span class="text-danger">( * )</span>
                                        </b>
                                    </h5>
                                </label>
                                <input type="number"
                                       name="quantity_pc"
                                       id="quantity_pc"
                                       class="form-control"
                                       value="{{ old('quantity_pc') }}"
                                       placeholder="Cantidad por cortar" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="material_id">
                                    <h5>
                                        <b>
                                            Material <span class="text-danger">( * )</span>
                                        </b>
                                    </h5>
                                </label>
                                <br>
                                <select id="material_id" name="material_id" class="form-control materials-select2"
                                        required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="machinery_id">
                                    <h5>
                                        <b>
                                            Maquinaría <span class="text-danger">( * )</span>
                                        </b>
                                    </h5>
                                </label>
                                <br>
                                <select id="machinery_id" name="machinery_id" class="form-control machineries-select2"
                                        required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 mb-2">
                            <label for="comment" class="mb-2 pb-1">
                                <h5>
                                    <b>
                                        Prioridad <span class="text-danger">( * )</span>
                                    </b>
                                </h5>
                            </label>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input"
                                       id="inlineRadio1"
                                       type="radio"
                                       name="priority"
                                       required
                                       value="1">
                                <label class="form-check-label" for="inlineRadio1"><h5 class="mb-0">1</h5></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input"
                                       id="inlineRadio2"
                                       type="radio"
                                       name="priority"
                                       required
                                       value="2">
                                <label class="form-check-label" for="inlineRadio2"><h5 class="mb-0">2</h5></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input"
                                       id="inlineRadio3"
                                       type="radio"
                                       name="priority"
                                       required
                                       value="3">
                                <label class="form-check-label" for="inlineRadio3"><h5 class="mb-0">3</h5></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input"
                                       id="inlineRadio4"
                                       type="radio"
                                       name="priority"
                                       required
                                       value="4">
                                <label class="form-check-label" for="inlineRadio4"><h5 class="mb-0">4</h5></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input"
                                       id="inlineRadio5"
                                       type="radio"
                                       name="priority"
                                       required
                                       value="5">
                                <label class="form-check-label" for="inlineRadio5"><h5 class="mb-0">5</h5></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="description_material">
                                    <h5>
                                        <b>
                                            D - Material
                                        </b>
                                    </h5>
                                </label>
                                <input type="text"
                                       name="description_material"
                                       id="description_material"
                                       class="form-control"
                                       value="{{ old('description_material') }}"
                                       placeholder="Descripción del material">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="order_pt">
                                    <h5>
                                        <b>
                                            OP - terminado <span class="text-danger">( * )</span>
                                        </b>
                                    </h5>
                                </label>
                                <input type="number"
                                       name="order_pt"
                                       id="order_pt"
                                       class="form-control"
                                       value="{{ old('order_pt') }}"
                                       placeholder="Número de orden del producto terminado">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="client_name">
                                    <h5>
                                        <b>
                                            N - Cliente <span class="text-danger">( * )</span>
                                        </b>
                                    </h5>
                                </label>
                                <input type="text"
                                       name="client_name"
                                       id="client_name"
                                       class="form-control"
                                       value="{{ old('client_name') }}"
                                       placeholder="Nombre del cliente" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="entry_date">
                                <h5>
                                    <b>
                                        Fecha de entrada <span class="text-danger">( * )</span>
                                    </b>
                                </h5>
                            </label>
                            <br>
                            <div class="input-group">
                                <input class="form-control date datepicker"
                                       type="text"
                                       aria-describedby="basic-addon2"
                                       name="entry_date" id="entry_date" required
                                       value="{{old('entry_date')}}">
                                <span class="input-group-text" id="basic-addon2">
                                    <i class="fas fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="date_delivery">
                                <h5>
                                    <b>
                                        Fecha de disposición <span class="text-danger">( * )</span>
                                    </b>
                                </h5>
                            </label>
                            <br>
                            <div class="input-group">
                                <input class="form-control date datepicker"
                                       type="text"
                                       aria-describedby="basic-addon2"
                                       name="date_delivery" id="date_delivery" required
                                       value="{{old('date_delivery')}}">
                                <span class="input-group-text" id="basic-addon2">
                                    <i class="fas fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mt-3">
                            <div class="form-group">
                                <label for="description">
                                    <h5>
                                        <b>
                                            Descripción de la tarea
                                        </b>
                                    </h5>
                                </label>
                                <br>
                                <textarea name="description"
                                          id="description"
                                          rows="3"
                                          class="form-control">{{old('description')}}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mt-3">
                            <div class="form-group">
                                <label for="comment">
                                    <h5>
                                        <b>
                                            Comentarios a tener en cuenta
                                        </b>
                                    </h5>
                                </label>
                                <br>
                                <textarea name="comment"
                                          id="comment"
                                          rows="3"
                                          class="form-control">{{old('comment')}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
