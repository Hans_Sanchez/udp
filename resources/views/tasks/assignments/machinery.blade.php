<!-- Modal -->
<div class="modal fade" id="machinery" tabindex="-1" role="dialog" aria-labelledby="machineryTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title bg-espumados"
                    id="machineryLongTitle">
                    Identificador de maquinaria (Ventana informativa)
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bv-modal">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-striped table-condensed">
                        <thead class="bg-orange headerStatic">
                        <tr class="text-center">
                            <th class="tt-espumados">
                                Identificador para carga masiva
                            </th>
                            <th class="tt-espumados">
                                Nombre de la maquina
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center" style="background: #FFF !important;">
                            @foreach($machineries as $machinery)
                                <td>{{ $machinery->id }}</td>
                                <td>{{ $machinery->name }}</td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
