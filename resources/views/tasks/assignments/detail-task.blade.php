<!-- Modal -->
<div class="modal fade" id="detailTask-{{ $assigmentTask->id }}" tabindex="-1"
     aria-labelledby="detailTask-{{ $assigmentTask->id }}-Label" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title bg-espumados text-uppercase"
                    id="detailTask-{{ $assigmentTask->id }}-Label">
                    Detalle de la tarea #{{ $assigmentTask->id }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bv-modal" style="text-align: left !important;">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="order_sem" class="bg-espumados">
                                <h5>
                                    <b>
                                        N° Orden de Semi-Elaborados
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->order_sem }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="blq" class="bg-espumados">
                                <h5>
                                    <b>
                                        Bloque
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->blq }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="order_sem" class="bg-espumados">
                                <h5>
                                    <b>
                                        Cantidad por cortar
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->quantity_pc }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="material_id" class="bg-espumados">
                                <h5>
                                    <b>
                                        Material
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->material->name }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="machinery_id" class="bg-espumados">
                                <h5>
                                    <b>
                                        Maquinaría
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->machinery->name }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 mb-2">
                        <label for="comment" class="mb-2 pb-1 bg-espumados">
                            <h5>
                                <b>
                                    Prioridad
                                </b>
                            </h5>
                        </label>
                        <h5>Prioridad: {{ $assigmentTask->priority }}</h5>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="description_material" class="bg-espumados">
                                <h5>
                                    <b>
                                        Descripción del Material
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->description_material }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="order_pt" class="bg-espumados">
                                <h5>
                                    <b>
                                        N° de Producto terminado
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->order_pt }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="client_name" class="bg-espumados">
                                <h5>
                                    <b>
                                        Nombre del Cliente
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->client_name }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label for="entry_date" class="bg-espumados">
                            <h5>
                                <b>
                                    Fecha de entrada
                                </b>
                            </h5>
                        </label>
                        <h5><b>{{ date('d-m-Y', strtotime($assigmentTask->entry_date)) }}</b></h5>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label for="date_delivery" class="bg-espumados">
                            <h5>
                                <b>
                                    Fecha de disposición
                                </b>
                            </h5>
                        </label>
                        <h5><b>{{ date('d-m-Y', strtotime($assigmentTask->date_delivery)) }}</b></h5>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mt-3">
                        <div class="form-group">
                            <label for="description" class="bg-espumados">
                                <h5>
                                    <b>
                                        Descripción de la tarea
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ $assigmentTask->description }}</b></h5>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mt-3">
                        <div class="form-group">
                            <label for="comment" class="bg-espumados">
                                <h5>
                                    <b>
                                        Comentarios a tener en cuenta
                                    </b>
                                </h5>
                            </label>
                            <h5><b>{{ ($assigmentTask->comment != null) ? $assigmentTask->comment : 'Sin comentarios adicionales' }}</b></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
