<!-- Modal -->
<div class="modal fade" id="createMassive" tabindex="-1" aria-labelledby="createMassiveLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title bg-espumados text-uppercase" id="createMassiveLabel">
                    Carga masiva de tareas
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('tasks.assigment.import')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body bv-modal">
                    <div class="row">
                        <div class="col-md-10 ml-auto mr-auto">
                            <blockquote class="blockquote">
                                Por favor seleccione el archivo a subir, tenga en cuenta que:
                                <strong>El archivo debe ser .XLSX</strong>
                                <br>
                            </blockquote>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="file"
                                       class="form-control"
                                       name="file"
                                       style="height: 41px !important;"
                                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                       required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Cargar datos
                        </button>
                        <a href="{{ asset('documents/example-tasks.xlsx') }}"
                           download="archivo-de-ejemplo"
                           class="btn btn-success"
                           title="Descargar"
                           style="cursor: pointer !important; color: white !important;">
                            <img src="{{asset('assets/img/icons8-microsoft-excel-22.png')}}"
                                 width="18px"
                                 height="18px"
                                 style="display: inline !important;"
                                 alt="icons8-microsoft-excel-22.png">
                            Plantilla de ejemplo
                        </a>
                </div>
            </form>
        </div>
    </div>
</div>
