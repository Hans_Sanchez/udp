<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <base href="./">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
        <meta name="author" content="Łukasz Holeczek">
        <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
        <title>UDP</title>
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/config_100.png') }}">
        <link rel="manifest" href="{{ asset('assets/favicon/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('assets/img/config_100.png') }}">
        <meta name="theme-color" content="#ffffff">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

        <!-- Scripts -->
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://kit.fontawesome.com/84d38f548e.js" crossorigin="anonymous"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{asset('js/password.js')}}"></script>
    </head>
    <body>
        <div class="bg-light min-vh-100 d-flex flex-row align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </body>
</html>
