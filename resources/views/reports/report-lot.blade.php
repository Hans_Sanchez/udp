<table class="table">
    <thead>
    <tr>
        <th><b>Material de formación</b></th>
        <th><b>Altura PL - (CM)</b></th>
        <th><b>Longitud PL - (MTS)</b></th>
        <th><b>Volumen PL - (M<sup>3</sup>)</b></th>
        <th><b>Cúbicaje PL - (KG)</b></th>
        <th><b>Producción parcial - (UND)</b></th>
        <th><b>Bloques anulados - (UND)</b></th>
        <th><b>Producción real - (UND)</b></th>
    </tr>
    <tr>
        <td>{{ $request['material']['name'] }}</td>
        <td>{{ number_format($request['average_height'], 0) }}</td>
        <td>{{ number_format($request['average_long'], 0) }}</td>
        <td>{{ number_format($request['average_volume'], 0) }}</td>
        <td>{{ number_format($request['average_cubic'], 0) }}</td>
        <td>{{ number_format($request['partial_production']) + number_format($request['overlooked']) }}</td>
        <td>{{ number_format($request['overlooked']) }}</td>
        <td>{{ number_format($request['partial_production']) }}</td>
    </tr>
    </thead>
    <thead>
    <tr>
        <th><b>Consecutivo del bloque</b></th>
        <th><b>Altura en reportada (CM)</b></th>
        <th><b>Altura en frío (CM)</b></th>
        <th><b>Largo (CM)</b></th>
        <th><b>Ancho (CM)</b></th>
        <th><b>Volumen (M<sup>3</sup>)</b></th>
        <th><b>Cúbicaje (KG)</b></th>
        <th><b>Anulado</b></th>
        <th><b>Observaciones</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($lot as $key => $item)
        <tr>
            <td>#{{ $item->id }}</td>
            <td>{{ number_format( $item->hot_height, 0) }}</td>
            <td>{{ number_format( $item->cold_height, 0) }}</td>
            <td>{{ number_format( $item->block_length, 0) }}</td>
            <td>{{ number_format( $item->width, 0) }}</td>
            <td>{{ number_format( $item->volume, 0) }}</td>
            <td>{{ number_format( $item->cubed, 0) }}</td>
            <td>{{ $item->canceled ? 'SI' : 'NO' }}</td>
            <td>{{ ($item->observations == null) ? '-o-' : $item->observations }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{--@dd(1)--}}
