<table class="table">
    <thead>
    <tr>
        <th class="tt-espumados">Puesto de trabajo al que trasladan</th>
        <th class="tt-espumados">Sub área al que lo están trasladando</th>
        <th class="tt-espumados">El que reporta</th>
        <th class="tt-espumados">El que recibe</th>
        <th class="tt-espumados">El que distribuye</th>
        <th class="tt-espumados">El que los traslada</th>
        <th class="tt-espumados">Observaciones en el translado</th>
        <th class="tt-espumados">Estado del traslado</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $transfer->workspace }}</td>
        <td>{{ $transfer->subArea->name }}</td>
        <td>{{ $transfer->user_inform_name }}</td>
        <td>{{ $transfer->user_receive_name }}</td>
        <td>{{ $transfer->user_distribute_name }}</td>
        <td>{{ $transfer->user_move_name }}</td>
        <td>{{ $transfer->observations }}</td>
        <td>{{ $transfer->status_report_label }}</td>
    </tr>
    </tbody>
</table>
<table class="table">
    <thead>
    <tr>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="8">BLOQUES TRASLADADOS</td>
    </tr>
    </tbody>
</table>
<table class="table">
    <thead>
    <tr>
        <th>Lote</th>
        <th>Código o consecutivo del sistema</th>
        <th>Código o consecutivo manual de producción</th>
        <th>Largo</th>
        <th>Ancho</th>
        <th>Altura en frío o Seco</th>
        <th>Cubicaje</th>
        <th>Anulado</th>
        <th>Observaciones en el bloque</th>
        <th>Código SAP del material de formación</th>
        <th>Material de formación</th>
        <th>Densidad</th>
    </tr>
    </thead>
    <tbody>
    @foreach($transfer->BlocksTransfers as $key => $item)
        <tr>
            <td>{{$item->lot->year . '-' . $item->lot->consecutive_manual }}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->consecutive_manual}}</td>
            <td>{{$item->block_length}}<b>CM</b></td>
            <td>{{$item->width}}<b>CM</b></td>
            <td>{{$item->cold_height}}<b>CM</b></td>
            <td>{{$item->cubed}}<b>Kg</b></td>
            <td>{{$item->canceled ? 'SI' : 'NO'}}</td>
            <td>{{$item->observations}}</td>
            <td>{{$item->lot->material->code}}</td>
            <td>{{$item->lot->material->name}}</td>
            <td>{{$item->lot->material->density}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{--@dd(1)--}}
