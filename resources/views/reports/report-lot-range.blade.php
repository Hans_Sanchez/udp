<table class="table">
    <thead>
    <tr>
        <th>Lote</th>
        <th>Material de formación</th>
        <th>Altura PL<b> - (CM)</b></th>
        <th>Longitud PL<b> - (MTS)</b></th>
        <th>Volumen PL<b> - (M<sup>3</sup>)</b></th>
        <th>Cúbicaje PL<b> - (Kg)</b></th>
        <th>Curado PL<b> - (CM)</b></th>
        <th>Producción parcial<b> - (UND)</b></th>
        <th>Bloques anulados<b> - (UND)</b></th>
        <th>Producción real<b> - (UND)</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($report as $key => $item)
        <tr>
            <td>{{ $item->year . '-' . $item->id }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ number_format($item->average_height, 0) }}</td>
            <td>{{ number_format($item->block_length / 100) }}</td>
            <td>{{ number_format($item->average_volume, 0) }}</td>
            <td>{{ number_format($item->average_cubic, 0) }}</td>
            <td>{{ number_format($item->shrinkage) }}</td>
            <td>{{ number_format($item->registers) }}</td>
            <td>{{ number_format($item->overlooked) }}</td>
            <td>{{ number_format(($item->registers - $item->overlooked)) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{--@dd(1)--}}
