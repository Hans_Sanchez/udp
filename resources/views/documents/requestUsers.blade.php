<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SOLICITUD #{{ $requestUser->id }}</title>
    <link rel="manifest" href="{{ asset('assets/favicon/manifest.json') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        body {
            font-family: Arial, Helvetica, Verdana;
        }

        table * {
            font-size: 10px !important;
        }

        p, li * {
            font-size: 8px !important;
        }

        table th, table td, .table td, .table th {
            padding: 1px !important;
            height: auto !important;
            border: none !important;
        }

        .table-bordered-bd-black td, .table-bordered-bd-black th {
            border: 1px solid #1a2035 !important;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }

        th {
            border: none !important;
            text-align: inherit;
        }

        .table {
            border: none !important;
            border-collapse: collapse;
        }

        .table {
            float: right !important;
            width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
        }

        .text-center {
            text-align: center;
        }

        .text-justify {
            text-align: justify !important;
        }


        .table-borderless td {
            border: none !important;
        }


        img#logo {
            /* Arriba | Derecha | Abajo | Izquierda */
            margin: 10px 10px 20px 265px;
        }

        #resolution {
            font-size: 14px !important;
        }

        .space {
            margin-bottom: -5px !important;
        }

        p#context {
            font-size: 13px !important;
            font-style: italic !important;
        }

        b#direct {
            font-size: 12px !important;
            margin-bottom: 5px !important;
        }

        p.text-size-description {
            font-size: 12px !important;
            margin-bottom: -10px !important;
        }

        p.text-size-description-u {
            font-size: 12px !important;
        }

        b#considering {
            font-size: 12px !important;
        }

        p#text-size {
            font-size: 12px !important;
        }

        p#text-size-italic {
            font-size: 12px !important;
            font-style: italic;
        }

        p#text-direct {
            margin-top: 0 !important;
            font-size: 12px !important;
        }
        p.final{
            text-transform: uppercase !important;
            margin-bottom: 0 !important;
        }


    </style>
</head>
<body>
<div>
    <header id="header">
        <img src="{{ $logo }}"
             width="180"
             height="110"
             id="logo">
    </header>
    <table class="table">
        <tbody>
        <tr>
            <td class="text-center">
                <b id="resolution">RESOLUCIÓN</b>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <b style="font-size: 12px !important;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</b>
            </td>
        </tr>
        <tr>
            <td class="space"></td>
        </tr>
        <tr>
            <td class="text-center">
                <p id="context">“Por la cual se ordena la cancelación de
                    un {{ $requestUser->document_label }}"</p>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <b id="direct">EL DIRECTOR NACIONAL DE REGISTRO CIVIL</b>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <p class="text-size-description">En ejercicio de las facultades legales conferidas</p>
                <p class="text-size-description">mediante Resolución No. 1970 del 09 de</p>
                <p class="text-size-description-u">junio de 2003 y</p>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <b id="considering">CONSIDERANDO:</b>
            </td>
        </tr>
        <tr>
            <td class="text-justify">
                <p id="text-size">Que de conformidad con el artículo 266 de la Constitución Política,
                    modificado por el
                    Acto
                    Legislativo 01 del 3 de julio de 2003, corresponde al Registrador Nacional del Estado
                    Civil, la
                    dirección y organización del registro civil.</p>
                <p id="text-size">Que el numeral 9° del artículo 40 del Decreto 1010 del 6 de junio de 2000
                    asigna al
                    Director
                    Nacional de Registro Civil entre otras funciones, la de expedir resoluciones de
                    cancelación de
                    registros civiles cuando el hecho o el acto ya se encontrare registrado, según lo
                    dispuesto en el
                    inciso segundo del artículo 65 del Decreto Ley 1260 del 27 de julio de 1970: </p>
                <p id="text-size-italic">“La Oficina Central dispondrá la cancelación de la inscripción,
                    cuando
                    compruebe que la persona
                    objeto de ella ya se encuentra registrada”.</p>
                <p id="text-size">Que <b style="font-size: 12px !important;">{{ $requestUser->name }}</b>,
                    debidamente identificado, presenta solicitud expresa de la cancelación de
                    un {{ $requestUser->document_label }}, que se relacionan a continuación:</p>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table table-bordered-bd-black">
                    <thead>
                    <tr>
                        <th class="text-center"><b>Inscrito</b></th>
                        <th class="text-center"><b>Serial</b></th>
                        <th class="text-center"><b>Oficina de Registro</b></th>
                        <th class="text-center"><b>Fecha de Inscripción</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">{{ $requestUser->r_to_c }}</td>
                        <td class="text-center">{{ $requestUser->s_to_c }}</td>
                        <td class="text-center">{{ $requestUser->o_to_c }}</td>
                        <td class="text-center">{{ $requestUser->f_to_c }}</td>
                    </tr>
                    <tr>
                        <td class="text-center">{{ $requestUser->r_to_u }}</td>
                        <td class="text-center">{{ $requestUser->s_to_u }}</td>
                        <td class="text-center">{{ $requestUser->o_to_u }}</td>
                        <td class="text-center">{{ $requestUser->f_to_u }}</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td class="text-justify" style="margin-top: 45px !important;">
                <p id="text-size">Que con el fin de comprobar la doble inscripción de la que fue objeto la
                    persona anteriormente relacionada, aporta copias de los registros en mención,
                    verificándose con los datos del Servicio Nacional de Inscripción que estos documentos
                    evidencian que se trata de la misma persona que fue registrada doblemente, toda vez que
                    se encuentran consignados los mismos datos biográficos</p>
                <p id="text-size">Que se hace necesario proceder a la cancelación de una de las inscripciones con el fin
                    de dar cumplimiento a la norma en cuanto a que el registro debe ser único y definitivo. </p>
                <p id="text-size">Que el numeral 11 del artículo 3 del Código de Procedimiento Administrativo y de lo
                    Contencioso
                    Administrativo – Ley 1437 de 2011 establece que las actualizaciones administrativas se desarrollan
                    con arreglo, entre otros, al principio de eficacia, en virtud del cual “las autoridades buscarán que
                    los procedimientos logren su finalidad y, para el efecto, removerán de oficio los obstáculos
                    puramente formales, evitaran decisiones inhibitorias dilaciones o retardos y sanearan, de acuerdo
                    con este Código las irregularidades procedimentales que se presenten en procura de la efectividad
                    del derecho material objeto de la actuación administrativa”.</p>
                <p id="text-size">Por lo anterior, se da aplicación al artículo 83 de la Constitución Política “Las
                    actuaciones de los
                    particulares y de las autoridades públicas deberán ceñirse a los postulados de la buena fe, la cual
                    se presumirá en todas las gestiones que aquellos adelanten ante éstas”, entendiendo que lo
                    manifestado en el escrito corresponde a la verdad. Si a futuro, por hechos sobrevinientes, se
                    comprobara que la información aportada no fue fidedigna, se adelantarán las actuaciones legales
                    correspondientes. </p>
                <p id="text-size">En mérito de lo expuesto, la Dirección Nacional de Registro Civil:</p>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <b id="considering">RESUELVE</b>
            </td>
        </tr>
        <tr>
            <td class="text-justify">
                <p id="text-size"><b>ARTÍCULO PRIMERO</b> Ordenar la cancelación del registro civil de nacimiento que se
                    relaciona a continuación:</p>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table table-bordered-bd-black">
                    <thead>
                    <tr>
                        <th class="text-center"><b>Inscrito</b></th>
                        <th class="text-center"><b>Serial</b></th>
                        <th class="text-center"><b>Oficina de Registro</b></th>
                        <th class="text-center"><b>Fecha de Inscripción</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">{{ $requestUser->r_to_u }}</td>
                        <td class="text-center">{{ $requestUser->s_to_u }}</td>
                        <td class="text-center">{{ $requestUser->o_to_u }}</td>
                        <td class="text-center">{{ $requestUser->f_to_u }}</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td class="text-justify" style="margin-top: 25px !important;">
                <p id="text-size">En la casilla de notas o al margen de la inscripción que se ordena cancelar, se
                    suscribirá y anotará por el funcionario competente el número y la fecha de esta resolución.</p>
            </td>
        </tr>
        <tr>
            <td class="text-justify">
                <p id="text-size"><b>PARÁGRAFO:</b> Enviar copia de la presente Resolución para su cumplimiento al
                    funcionario responsable de la oficina de registro y al Coordinador del Servicio Nacional de
                    Inscripción de la Dirección Nacional de Registro Civil.</p>
            </td>
        </tr>
        <tr>
            <td class="text-justify">
                <p id="text-size"><b>ARTÍCULO SEGUNDO:</b> Notificar el presente acto administrativo, conforme lo
                    establece el artículo 66 y siguientes del Código de Procedimiento Administrativo y de lo Contencioso
                    Administrativo (Ley 1437 del 18 de enero de 2011)</p>
            </td>
        </tr>
        <tr>
            <td class="text-justify">
                <p id="text-size"><b>ARTÍCULO TERCERO:</b> Contra la presente Resolución proceden los recursos de
                    reposición ante el Director Nacional de Registro Civil y de apelación ante el Registrador Delegado
                    para el Registro Civil y la Identificación dentro de los diez (10) días hábiles siguientes a su
                    notificación en los términos de los artículos 74, 76 y 77 de la Ley 1437 de 2011.</p>
            </td>
        </tr>
        <tr>
            <td class="text-justify">
                <p id="text-size">
                    <b>ARTÍCULO CUARTO:</b> Esta Resolución rige a partir de la fecha de su ejecutoria.
                </p>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <b id="considering">NOTIFÍQUESE Y CÚMPLASE</b>
            </td>
        </tr>
        <tr>
            <td class="text-justify">
                <p id="text-size">
                    Dada en Bogotá D. C. el {{ date('d-m-Y' , strtotime($requestUser->updated_at)) }} a
                    las {{ date('g:i a' , strtotime($requestUser->updated_at)) }}
                </p>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <b id="considering">RODRIGO PÉREZ MONROY</b>
                <p id="text-direct">Director Nacional de Registro Civil</p>
            </td>
        </tr>
        <tr>
            <td class="text-left">
                <p class="final">
                    <b>APROBÓ:</b> {{ $requestUser->pass_label ?: '-0-'}}
                </p>
                <p>Coordinadora Grupo Jurídica de Registro Civil
                </p>
                <p class="final">
                    <b>REVISÓ:</b> {{ $requestUser->check_label ?: '-0-'}}
                </p>
                <p class="final">
                    <b>ELABORÓ:</b> {{ $requestUser->elaborate ?: '-0-'}}
                </p>
                <p class="final">
                    <b>RADICADO:</b>
                    {{ $requestUser->id }}/{{ date('Y' , strtotime($requestUser->created_at)) }}
                </p>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
