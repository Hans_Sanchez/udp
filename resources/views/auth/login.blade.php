<x-guest-layout>
    <div class="col-lg-8" id="app">
        <div class="card-group d-block d-md-flex row">
            <div class="card col-md-12 p-4 mb-0">
                <div class="card-body">
                    @if ($errors->has('email'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> {{ $errors->first('email') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if ($errors->has('password'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> {{ $errors->first('password') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="text-center">
                        <p class="title-login">Inicio de sesión</p>
                    </div>
                    <hr>
                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        <div class="input-group mb-3">
                            <span class="input-group-text">
                                <i class="fas fa-user"></i>
                            </span>
                            <input type="email" name="email" class="form-control" id="email"
                                   placeholder="tucorreo@grupoespumadossa.com.co" autofocus required>
                        </div>
                        <div class="input-group mb-4" style="cursor: pointer !important;">
                            <span class="input-group-text">
                                <i class="fas fa-lock"></i>
                            </span>
                            <input type="password" class="form-control password-login" id="password-login"
                                   placeholder="MiClave123" name="password" required>
                            <span class="input-group-text eye-hide" id="eye-password-login">
                                <i class="fas fa-eye-slash"></i>
                            </span>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <button class="btn btn-primary w-100" type="submit">Inicio de sesión</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
