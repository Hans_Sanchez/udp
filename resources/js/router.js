import Vue from 'vue'
import Router from 'vue-router'
import home from './views_vue/Home.vue'
import users from './views_vue/administration/Employees.vue'
import requests from './views_vue/administration/RequestUsers.vue'


Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {path: '/v/home', name: 'home', component: home},
        {path: '/v/users', name: 'users', component: users},
        {path: '/v/requests', name: 'requests', component: requests},
    ]
})
