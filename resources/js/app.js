require('./bootstrap');

// Require Vue
window.Vue = require('vue').default;

require('@coreui/coreui/dist/js/coreui.bundle.min');

//rutas vue
import router from './router';

import VueRouter from 'vue-router'

Vue.use(VueRouter)

import vSelect from "vue-select";

Vue.component('v-select', vSelect)

import PulseLoader from 'vue-spinner/src/PulseLoader.vue'

Vue.component('pulse-loader', PulseLoader);

import InfiniteLoading from 'vue-infinite-loading';

Vue.use(InfiniteLoading, { /* options */});

import toastr from "toastr";

window.toastr = toastr;

// import plugin
import VueToastr from "vue-toastr";

Vue.use(VueToastr);

import VueSweetalert2 from 'vue-sweetalert2';
import VueMask from 'v-mask'

// Las 3 son requeridad para que valide si es requerido y el tipo de campo email
import {ValidationProvider} from 'vee-validate';
import {extend} from 'vee-validate';
import {required, email} from 'vee-validate/dist/rules';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
import 'vue-select/dist/vue-select.css';

// PAQUETES EXTERNOS
Vue.use(VueSweetalert2);
Vue.component('v-select', vSelect)
Vue.use(VueMask);
Vue.component('ValidationProvider', ValidationProvider);

import VueNumberFormat from 'vue-number-format'

Vue.use(VueNumberFormat, {
    prefix: '',
    suffix: '',
    decimal: ',',
    thousand: '.',
    precision: 0,
    acceptNegative: true,
    isInteger: false
})


const moment = require('moment/locale/es')
Vue.use(require('vue-moment'), {
    moment
})
// Register Vue Components
Vue.component('content-component', require('./components/ContentComponent.vue').default);
Vue.component('graphics-component', require('./components/Graphics.vue').default);


extend('required', {
    ...required,
    message: 'Este campo es requerido!'
});

// Add the email rule
extend('email', {
    ...email,
    message: 'El campo no tiene un formato de correo correcto, verifique por favor'
});

// Initialize Vue
export const appVue = new Vue({
    el: '#app',
    router,
    data: {
        show: true
    }
});

// window.VueRef=appVue.$refs;
