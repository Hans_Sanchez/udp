<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Authentication
Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::get('/', function () {
    return redirect()->route('login');
});

Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'admin'], function () {
        Voyager::routes();
    });

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::group(['prefix' => 'udp'], function () {
        Route::group(['prefix' => 'administration'], function () {
            Route::group(['prefix' => 'request'], function () {
                Route::get('/getRequest', [\App\Http\Controllers\RequestUserController::class, 'getRequest']);
                Route::post('/store', [\App\Http\Controllers\RequestUserController::class, 'store']);
                Route::post('/{requestUser}/update', [\App\Http\Controllers\RequestUserController::class, 'update']);
                Route::get('/generatePDF/{requestUser}', [\App\Http\Controllers\RequestUserController::class, 'generatePDF']);
            });

            Route::group(['prefix' => 'users'], function () {
                Route::get('/getUsers', [\App\Http\Controllers\UserController::class, 'getUsers']);
                Route::get('/getUsersList', [\App\Http\Controllers\UserController::class, 'getUsersList']);
                Route::post('/store', [\App\Http\Controllers\UserController::class, 'store']);
                Route::post('/{user}/update', [\App\Http\Controllers\UserController::class, 'update']);
                Route::post('/importUsers', [\App\Http\Controllers\UserController::class, 'import'])->name('users.import');
            });
        });

    });

    Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('/v/{any?}/{any1?}/{any2?}/{any3?}/{any4?}', function ($any = null, $any1 = null, $any2 = null, $any3 = null, $any4 = null) {
        return view('layouts.coreui');
    })->where('vue', '.*')->name('rutas.vue');
});
