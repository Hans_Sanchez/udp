$(document).ready(function () {
    $('#eye-password-login').click(function () {
        if ($('#eye-password-login').hasClass('eye-hide')) {
            $('#eye-password-login').removeClass('eye-hide');
            $('#eye-password-login').addClass('eye-show');
            $('#eye-password-login i').removeClass('fa-eye-slash');
            $('#eye-password-login i').addClass('fa-eye');
            $('.password-login').attr('type', 'text');
        }else{
            $('#eye-password-login').removeClass('eye-show');
            $('#eye-password-login').addClass('eye-hide');
            $('#eye-password-login i').removeClass('fa-eye');
            $('#eye-password-login i').addClass('fa-eye-slash');
            $('.password-login').attr('type', 'password');
        }
    });

    $('#eye-password-create').click(function () {
        if ($('#eye-password-create').hasClass('eye-hide')) {
            $('#eye-password-create').removeClass('eye-hide');
            $('#eye-password-create').addClass('eye-show');
            $('#eye-password-create i').removeClass('fa-eye-slash');
            $('#eye-password-create i').addClass('fa-eye');
            $('.password-create').attr('type', 'text');
        }else{
            $('#eye-password-create').removeClass('eye-show');
            $('#eye-password-create').addClass('eye-hide');
            $('#eye-password-create i').removeClass('fa-eye');
            $('#eye-password-create i').addClass('fa-eye-slash');
            $('.password-create').attr('type', 'password');
        }
    });
    $('#eye-password-create-confirmation').click(function () {
        if ($('#eye-password-create-confirmation').hasClass('eye-hide')) {
            $('#eye-password-create-confirmation').removeClass('eye-hide');
            $('#eye-password-create-confirmation').addClass('eye-show');
            $('#eye-password-create-confirmation i').removeClass('fa-eye-slash');
            $('#eye-password-create-confirmation i').addClass('fa-eye');
            $('.password-confirmation-create').attr('type', 'text');
        }else{
            $('#eye-password-create-confirmation').removeClass('eye-show');
            $('#eye-password-create-confirmation').addClass('eye-hide');
            $('#eye-password-create-confirmation i').removeClass('fa-eye');
            $('#eye-password-create-confirmation i').addClass('fa-eye-slash');
            $('.password-confirmation-create').attr('type', 'password');
        }
    });

    $('#eye-password-profile').click(function () {
        if ($('#eye-password-profile').hasClass('eye-hide')) {
            $('#eye-password-profile').removeClass('eye-hide');
            $('#eye-password-profile').addClass('eye-show');
            $('#eye-password-profile i').removeClass('fa-eye-slash');
            $('#eye-password-profile i').addClass('fa-eye');
            $('.password-profile').attr('type', 'text');
        }else{
            $('#eye-password-profile').removeClass('eye-show');
            $('#eye-password-profile').addClass('eye-hide');
            $('#eye-password-profile i').removeClass('fa-eye');
            $('#eye-password-profile i').addClass('fa-eye-slash');
            $('.password-profile').attr('type', 'password');
        }
    });
    $('#eye-password-profile-confirmation').click(function () {
        if ($('#eye-password-profile-confirmation').hasClass('eye-hide')) {
            $('#eye-password-profile-confirmation').removeClass('eye-hide');
            $('#eye-password-profile-confirmation').addClass('eye-show');
            $('#eye-password-profile-confirmation i').removeClass('fa-eye-slash');
            $('#eye-password-profile-confirmation i').addClass('fa-eye');
            $('.password-confirmation-profile').attr('type', 'text');
        }else{
            $('#eye-password-profile-confirmation').removeClass('eye-show');
            $('#eye-password-profile-confirmation').addClass('eye-hide');
            $('#eye-password-profile-confirmation i').removeClass('fa-eye');
            $('#eye-password-profile-confirmation i').addClass('fa-eye-slash');
            $('.password-confirmation-profile').attr('type', 'password');
        }
    });
});
